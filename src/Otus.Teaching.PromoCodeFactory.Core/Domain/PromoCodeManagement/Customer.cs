﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        [MaxLength(400)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public ICollection<CustomerPreference> Preferences { get; set; } = new List<CustomerPreference>();
        public ICollection<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();
    }
}