﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IEfRepository<T> : IRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        Task AddRangeAsync(List<T> entities);
        Task UpdateAsync(T entity);
        Task RemoveAsync(T entity);
        Task<int> SaveAsync();
    }
}