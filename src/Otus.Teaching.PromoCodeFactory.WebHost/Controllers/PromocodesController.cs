﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>Промокоды</summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IEfRepository<PromoCode> _repository;
        private readonly IEfRepository<Preference> _preferenceRepository;
        private readonly IEfRepository<Customer> _customerRepository;
        private readonly IEfRepository<Employee> _employeeRepository;


        public PromocodesController(
            IEfRepository<PromoCode> repository,
            IEfRepository<Preference> preferenceRepository,
            IEfRepository<Customer> customerRepository,
            IEfRepository<Employee> employeeRepository)
        {
            _repository = repository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>Получить все промокоды</summary>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync() =>
            Ok(
                (await _repository.GetAllAsync())
                    .Select(p => new PromoCodeShortResponse
                    {
                        Id = p.Id,
                        Code = p.Code,
                        PartnerName = p.PartnerName,
                        ServiceInfo = p.ServiceInfo,
                        BeginDate = p.BeginDate.ToString(),
                        EndDate = p.EndDate.ToString(),
                    })
            );

        /// <summary>Создать промокод и выдать его клиентам с указанным предпочтением</summary>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP404 - клиент не найден,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            // найти предпочтение в БД 
            var preference = (await _preferenceRepository.GetAllAsync())
                .FirstOrDefault(p => p.Name == request.Preference);
            if (preference == null)
                return BadRequest($"Предпочтение не найдено: {request.Preference}");
            // найти менеджера в БД
            var partnerManager = (await _employeeRepository.GetAllAsync())
                .FirstOrDefault(e => e.RoleId == Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"));
            if (partnerManager == null)
                return BadRequest($"Менеджер не найден");
            // клиент с минимальным количеством промокодов
            var minCustomer = (await _customerRepository.GetAllAsync())
                .Where(c => c.Preferences.Any(cp => cp.PreferenceId == preference.Id))
                .OrderBy(c => c.PromoCodes?.Count)
                .FirstOrDefault();
            if (minCustomer == null)
                return BadRequest($"Не найден подходящий клиент для промокода");

            var results = new List<string>();
            var promoCode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                PreferenceId = preference.Id,
                PartnerManagerId = partnerManager.Id,
                CustomerId = minCustomer.Id,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now + TimeSpan.FromDays(90)
            };
            results.Add($"Промокод присвоен клиенту [{minCustomer.Id}]");
            await _repository.AddAsync(promoCode);
            await _repository.SaveAsync();
            results.Add($"Создан промокод [{promoCode.Id}]");
            return Ok(results);
        }
    }
}
