﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>Клиенты</summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IEfRepository<Customer> _repository;
        private readonly IEfRepository<Preference> _preferenceRepository;


        public CustomersController(IEfRepository<Customer> repository, IEfRepository<Preference> preferenceRepository)
        {
            _repository = repository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>Получить список клиентов</summary>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _repository.GetAllAsync();
            return Ok(
                customers.Select(c => new CustomerShortResponse
                {
                    Id = c.Id,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Email = c.Email
                })
            );
        }

        /// <summary>Получить информацию о клиенте</summary>
        /// <param name="id">идентификатор клиента</param>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP404 - клиент не найден,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _repository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            return Ok(
                new CustomerResponse
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    PromoCodes = customer.PromoCodes?.Select(p => new PromoCodeShortResponse
                    {
                        Id = p.Id,
                        Code = p.Code,
                        PartnerName = p.PartnerName,
                        BeginDate = p.BeginDate.ToString(),
                        EndDate = p.EndDate.ToString(),
                        ServiceInfo = p.ServiceInfo
                    }).ToList(),
                    Preferences = customer.Preferences.Select(cp => cp.Preference.Name).ToList()
                }
            );
        }

        /// <summary>Добавить нового клиента</summary>
        /// <param name="request">данные для добавления</param>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP404 - клиент не найден,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null)
                return BadRequest("Нет данных для изменения");

            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            if (request.PreferenceIds != null)
            {
                var preferences = (await _preferenceRepository.GetAllAsync())
                    // взять только сущестующие в БД предпочтения
                    .Where(p => request.PreferenceIds.Contains(p.Id));

                foreach (var preference in preferences)
                {
                    customer.Preferences.Add(
                        new CustomerPreference
                        {
                            CustomerId = customer.Id,
                            Customer = customer,
                            PreferenceId = preference.Id,
                            Preference = preference
                        });
                }
            }

            await _repository.AddAsync(customer);
            await _repository.SaveAsync();
            return Ok(customer.Id);
        }

        /// <summary>Редактировать данные о имеющемся клиенте</summary>
        /// <param name="id">идентификатор клиента</param>
        /// <param name="request">данные для редактирования</param>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP404 - клиент не найден,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null)
                return BadRequest("Нет данных для изменения");

            var customer = await _repository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            if (request.FirstName != null)
                customer.FirstName = request.FirstName;
            if (request.LastName != null)
                customer.LastName = request.LastName;
            if (request.Email != null)
                customer.Email = request.Email;
            if (request.PreferenceIds != null)
            {
                var preferences = (await _preferenceRepository.GetAllAsync())
                    // взять только сущестующие в БД предпочтения
                    .Where(p => request.PreferenceIds.Contains(p.Id))
                    // взять еще не имеющиеся у клиента предпочтения
                    .Where(p => !customer.Preferences.Any(cp => cp.PreferenceId == p.Id));

                foreach (var preference in preferences)
                {
                    customer.Preferences.Add(
                        new CustomerPreference
                        {
                            CustomerId = customer.Id,
                            PreferenceId = preference.Id,
                        });
                }
            }

            await _repository.UpdateAsync(customer);
            await _repository.SaveAsync();
            return Ok();
        }

        /// <summary>Удалить клиента (каскадно вместе с промокодами)</summary>
        /// <param name="id">идентификатор клиента</param>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP404 - клиент не найден,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _repository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            await _repository.RemoveAsync(customer);
            await _repository.SaveAsync();
            return Ok();
        }
    }
}