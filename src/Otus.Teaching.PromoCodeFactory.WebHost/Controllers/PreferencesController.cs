﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>Предпочтения</summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IEfRepository<Preference> _repository;


        public PreferencesController(IEfRepository<Preference> repository) =>
            _repository = repository;

        /// <summary>Получить список предпочтений</summary>
        /// <remarks>
        ///     HTTP200 - успешно,
        ///     HTTP500 - внутренняя ошибка
        /// </remarks>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PreferenceResponse>), 200)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<IEnumerable<PreferenceResponse>>> GetAsync() =>
            Ok(
                 (await _repository.GetAllAsync())
                    .Select(p => new PreferenceResponse
                    {
                        Id = p.Id,
                        Name = p.Name
                    })
            );
    }
}
