﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataContexts;
using System;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection InitAndAddDataContext(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsAction)
        {
            // init db (code first) 
              var options = new DbContextOptionsBuilder<DataContext>();

            //optionsAction(options);
            //var init = new DataContext(options.Options);
            //init.Database.EnsureDeleted();
            //init.Database.EnsureCreated();

            // add scoped DbContext
            return services.AddDbContext<DataContext>(opt => optionsAction(opt));
        }
    }
}
