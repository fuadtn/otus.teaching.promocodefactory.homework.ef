﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Collections.Generic;
using System.Linq;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.DataContexts
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        

        public DataContext(DbContextOptions<DataContext> opt) : base(opt)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasKey(e => e.Id);
            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role);

            modelBuilder.Entity<Role>()
              .HasKey(r => r.Id);

            modelBuilder.Entity<Customer>()
              .HasKey(c => c.Id);
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes);
            modelBuilder.Entity<Customer>()
                .Property(c => c.FirstName)
                .HasMaxLength(768);

            modelBuilder.Entity<Preference>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.Preferences)
                .HasForeignKey(cp => cp.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany()
                .HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<PromoCode>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.PartnerManager);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Preference);


            // максимальная длина для всех строковых свойств
            foreach (var p in modelBuilder.Model
                .GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string)))
            {
                p.SetMaxLength(1024);
            }

            // заполнить тестовыми данными
            SeedData<Role>(modelBuilder, FakeDataFactory.Roles);
            SeedData<Employee>(modelBuilder, FakeDataFactory.Employees.ToList().Select(e =>
                new
                {
                    Id = e.Id,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    Email = e.Email,
                    AppliedPromocodesCount = e.AppliedPromocodesCount,
                    RoleId = e.Role.Id
                }));
            SeedData<Preference>(modelBuilder, FakeDataFactory.Preferences);
            SeedData<PromoCode>(modelBuilder, FakeDataFactory.PromoCodes.ToList().Select(p =>
                new
                {
                    Id = p.Id,
                    BeginDate = p.BeginDate,
                    EndDate = p.EndDate,
                    Code = p.Code,
                    PartnerManagerId = p.PartnerManager.Id,
                    PartnerName = p.PartnerName,
                    PreferenceId = p.Preference.Id,
                    CustomerId = FakeDataFactory
                                    .Customers
                                    .FirstOrDefault(c => c.PromoCodes.Any(cp => cp.Id == p.Id))?.Id
                }));
                SeedData<Customer>(modelBuilder, FakeDataFactory.Customers.ToList().Select(c =>
                new
                {
                    Id = c.Id,
                    Email = c.Email,
                    FirstName = c.FirstName,
                    LastName = c.LastName
                }));
        }

        protected void SeedData<TEntity>(ModelBuilder modelBuilder, IEnumerable<object> entities)
            where TEntity : class =>
            modelBuilder
                .Entity<TEntity>()
                .HasData(entities);
    }
}
