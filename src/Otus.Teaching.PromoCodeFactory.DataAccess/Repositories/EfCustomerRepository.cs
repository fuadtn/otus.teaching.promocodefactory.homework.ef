﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfCustomerRepository : EfRepository<Customer>
    {
        public EfCustomerRepository(DataContext dataContext) : base(dataContext) { }

        public override Task<IEnumerable<Customer>> GetAllAsync() =>
            Task.FromResult(
                _dataContext.Customers
                    .Include(c => c.PromoCodes)
                    .Include(c => c.Preferences)
                    .ToList() as IEnumerable<Customer>
                );

        public override Task<Customer> GetByIdAsync(Guid id) =>
            Task.FromResult(
                _dataContext.Customers
                    .Include(c => c.PromoCodes)
                    .Include(c => c.Preferences)
                    .ThenInclude(cp => cp.Preference)
                    .FirstOrDefault(c => c.Id == id)
            );
    }
}