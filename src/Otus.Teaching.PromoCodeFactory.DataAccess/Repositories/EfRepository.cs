﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IEfRepository<T> where T : BaseEntity
    {
        protected readonly DataContext _dataContext;


        public EfRepository(DataContext dataContext) =>
            this._dataContext = dataContext;

        public virtual Task<IEnumerable<T>> GetAllAsync() =>
            Task.FromResult<IEnumerable<T>>(this._dataContext.Set<T>().ToList());

        public virtual Task<T> GetByIdAsync(Guid id) =>
            Task.FromResult<T>(this._dataContext.Set<T>().FirstOrDefault(e => e.Id == id));

        public Task AddAsync(T entity) =>
            Task.Run(() =>
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));
                this._dataContext.Add(entity);
            });

        public Task AddRangeAsync(List<T> entities) =>
            Task.Run(() =>
            {
                if (entities == null)
                    throw new ArgumentNullException(nameof(entities));
                this._dataContext.AddRange(entities);
            });


        public Task UpdateAsync(T entity) =>
            Task.Run(() =>
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));
                this._dataContext.Update(entity);
            });

        public Task RemoveAsync(T entity) =>
            Task.Run(() =>
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));
                this._dataContext.Remove(entity);
            });

        public Task<int> SaveAsync() =>
            Task.FromResult(this._dataContext.SaveChanges());
    }
}