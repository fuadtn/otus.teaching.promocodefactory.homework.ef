﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class Migration0002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SomeProperty",
                table: "PromoCodes",
                maxLength: 1024,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("270cae60-a028-42d6-b287-636155df9014"),
                column: "BeginDate",
                value: new DateTime(2022, 5, 10, 21, 41, 24, 155, DateTimeKind.Local).AddTicks(5378));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SomeProperty",
                table: "PromoCodes");

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("270cae60-a028-42d6-b287-636155df9014"),
                column: "BeginDate",
                value: new DateTime(2022, 5, 10, 21, 37, 59, 311, DateTimeKind.Local).AddTicks(660));
        }
    }
}
